<?php
session_start();
$user = $_SESSION['user'];
?>
<!DOCTYPE html>
<html>

<head>
	<title>Connexion -- ChicouTruck</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link href="index.css" rel="stylesheet" type="text/css">
</head>

<body>

	<div class="background">

		<nav class="navbar navbar-expand-sm ml-auto bg-dark navbar-dark sticky-top">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="ProjetDeFinDeSession.php"><img src="img/logo.png" style="width: 150px;"></a>
				</li>
			</ul>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" href="#"><?= $user ?></a>
				</li>
				<li>
					<a class="nav-link" href="ProjetDeFinDeSession.php">Déconnexion</a>
				</li>
			</ul>
		</nav>

		<div id="info" class="rcoolfooter"></div>

		<div class="container" style="height: 682px">
			<div class="service-wrapper">
				<table id="serviceActif">
					<tr>
						<th>Nom du service</th>
						<th>Information</th>
					</tr>
					<tr>
						<td>Nom d'un service actif</td>
						<td><a href="info.php">INFO</a></td>
					</tr>
					<tr>
						<td>Nom d'un service actif</td>
						<td><a href="info.php">INFO</a></td>
					</tr>
				</table>
				<!--<input type="button" value="Demande de service" onclick="header('Location: demandeService.php ')" />-->
				<a href="demandeService.php">Demande de service</a>
				
			</div>
		</div>

		<div id="info" class="coolfooter"></div>

	</div>
	<div class="footer">
		<div class="row">
			<div class="col-1">
				<img src="img/logo.png" style="width: 150px;">
			</div>
			<div class="col-10">
				<p>Posted by: ChicouTruck</p>
				<p>Contact information: ChicouTruck@bellnet.ca</p>
			</div>
		</div>
	</div>
</body>

</html>