<!DOCTYPE html>
<html>

<head>
	<title>ChicouTruck</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link href="index.css" rel="stylesheet" type="text/css">
</head>

<body>

	<div class="background">
		<nav class="navbar navbar-expand-sm ml-auto bg-dark navbar-dark sticky-top">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="ProjetDeFinDeSession.php"><img src="img/logo.png" style="width: 150px;"></a>
				</li>
			</ul>
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="#Description">Description</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#info">Service Proposés</a>
				</li>
			</ul>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" href="connexion.php">Connexion</a>
				</li>
				<li>
					<a class="nav-link" href="inscription.php">Inscription</a>
				</li>
			</ul>
		</nav>

		<div id="Description" class="rcoolfooter"></div>

		<div class="container">
			<div id="section1">
				<div>
					<p>Depuis plus de 15 ans, ChicouTruck assure le transport des marchandises commerciales dans tout le
						Québec et l’Ontario. Un service de transport bien pensé, aux livraisons régulières et bien
						coordonnées...</p>
					<br>
					<p>Voilà qui accélère votre cycle d’affaires et optimise votre rendement !</p>
					<br>
					<p>Réception des marchandises, préparation de commande, gestion d’inventaire informatisée,
						expédition et livraison, nous offrons un service clés en main qui vous permet de concentrer vos
						efforts sur le développement de votre entreprise.</p>
					<br>
					<p>Notre division S.E.A. Logistique offre plus de 75 000 pieds carrés de surface d’entreposage pour
						des produits alimentaires secs ou réfrigérés ou pour tout autre produit. Un service de
						logistique assure la manipulation de vos biens selon les critères les plus rigoureux.</p>
				</div>
			</div>
		</div>

		<div id="info" class="coolfooter"></div>

		<div>
			<div class="jumbotron">
				<h1>Types de transport proposés :</h1>
				<div class="row">
					<div class="col-4">
						<h2 href="">Transport dédié</h2>
						<li>
							<b>Gestion de parcs</b>
						</li>
						<li>
							<b>Impartition</b>
						</li>
						<li>
							<b>Répartition</b>
						</li>
						<li>
							<b>Préparation de commande</b>
						</li>
						<li>
							<b>Canada – États-Unis – International</b>
						</li>
					</div>
					<div class="col-4">
						<h2 href="">Transport général</h2>
						<li>
							<b>Transport de charges partielles</b>
						</li>
						<li>
							<b>Transport de charges complètes</b>
						</li>
						<li>
							<b>Plus grande couverture LTL du Québec</b>
						</li>
						<li>
							<b>Technologies à la fine pointe</b>
						</li>
						<li>
							<b>Canada – États-Unis – International</b>
						</li>
					</div>
					<div class="col-4">
						<h2 href="">Transport réfrigéré</h2>
						<li>
							<b>Transport frais et congelé</b>
						</li>
						<li>
							<b>Préparation de commande</b>
						</li>
						<li>
							<b>Consolidation de volume</b>
						</li>
						<li>
							<b>Entreposage frigorifique</b>
						</li>
						<li>
							<b>Canada – États-Unis – International</b>
						</li>
					</div>
				</div>
				<h3 style="padding-top: 25px;">Quelle que soit la nature du produit à transporter, nous opérerons selon
					vos besoins.</h3>
			</div>
		</div>

		<div class="rcoolfooter"></div>

		<div class="container">
			<div id="section1">
				<p>Transporteur régional et provincial, situé à Laval, ChicouTruck a été fondé en 1996 et compte
					aujourd’hui une flotte d’une cinquantaine de camions et de remorques, plus de 60 employés et un
					réseau de fournisseurs de confiance avec lequel nous travaillons en étroite collaboration.</p>
				<br>
				<p>Pour un service clés en main, ChicouTruck complète ses services avec S.E.A. Logistique, sa division
					d’entreposage et de logistique des marchandises créée en 2005.</p>
				<br>
				<p>Ce qui nous distingue :</p>
				<br>
				<div style="padding-left: 100px">
					<li>
						<b>La synergie entre le transport et l’entreposage</b>
					</li>
					<li>
						<b>La rapidité d’exécution</b>
					</li>
					<li>
						<b>Un service clés en main</b>
					</li>
					<li>
						<b>Le professionnalisme</b>
					</li>
					<li>
						<b>Le service à la clientèle</b>
					</li>
					<li>
						<b>Un service 24 heures / 24, 7 jours / 7 </b>
					</li>
					<li>
						<b>L’aptitude à s’adapter aux aléas des échanges commerciaux</b>
					</li>
				</div>
				<br>
				<p>Notre mission : répondre aux besoins de nos clients en matière de transport, d’entreposage et de
					logistique selon les délais prévus et les standards de qualité les plus élevés.</p>
				<br>
				<p>Nos valeurs : Au sein de ChicouTruck, c’est le savoir-faire de notre personnel qui nous distingue de
					la concurrence. Notre équipe de professionnels enthousiastes travaille avec fiabilité et efficacité
					pour produire des résultats de qualité. Vous pouvez faire confiance à notre équipe dévouée pour bien
					refléter votre image.</p>
			</div>
		</div>
		<div class="coolfooter"></div>


		<div>
			<div class="jumbotron">
				<h1>ENTREPOSAGE ALIMENTAIRE ET DE MARCHANDISES</h1>
				<b>En matière d’équipement, nos vastes entrepôts sont équipés de A à Z et grâce à nos spécialistes de la
					logistique, nous offrons les services suivants :</b>
				<br>
				<br>
				<div>
					<li>
						<b>Cueillette de marchandise</b>
					</li>
					<li>
						<b>Réception des marchandises</b>
					</li>
					<li>
						<b>Préparation des commandes</b>
					</li>
					<li>
						<b>Suivi de l’inventaire informatisé conçu spécifiquement pour S.E.A. Logistique</b>
					</li>
					<li>
						<b>Centre de distribution </b>
					</li>
					<li>
						<b>Remballage, palettisation</b>
					</li>
					<li>
						<b>Expédition et livraison</b>
					</li>
					<li>
						<b>Entreposage conforme à des exigences de manutention spécifique</b>
					</li>
				</div>
			</div>
			<div class="rcoolfooter"></div>
		</div>

		<div class="container">
			<div id="section1">
				<p>S.E.A. Logistique se spécialise dans l’entreposage alimentaire mais offre aussi des services de
					location d’entrepôt pour d’autres types de produits secs. Le temps de rétention varie selon vos
					besoins (entreposage à court, moyen ou à long terme) et nos surfaces d’entreposage sont aérées,
					ordonnées et impeccablement propres. Une visite vous en convaincra.</p>
			</div>
		</div>
		<div class="coolfooter"></div>

	</div>

	<div class="footer">
		<div class="row">
			<div class="col-1">
				<img src="./img/logo.png" style="width: 150px;">
			</div>
			<div class="col-10">
				<p>Posted by: ChicouTruck</p>
				<p>Contact information: ChicouTruck@bellnet.ca</p>
			</div>
		</div>
	</div>

</body>

</html>