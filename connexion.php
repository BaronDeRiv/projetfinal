<?php
$msg = "";
if (isset($_GET["error"])) {
	$msg = "Nom ou mot de passe invalide";
}
?>
<!DOCTYPE html>
<html>

<head>
	<title>Connexion -- ChicouTruck</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link href="index.css" rel="stylesheet" type="text/css">
</head>

<body>
	<div class="background">
		<nav class="navbar navbar-expand-sm ml-auto bg-dark navbar-dark sticky-top">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="ProjetDeFinDeSession.php"><img src="img/logo.png" style="width: 150px;"></a>
				</li>
			</ul>
		</nav>

		<div id="info" class="rcoolfooter"></div>

		<div class="container" style="margin-bottom: 250px;">
			<div class="divConnect">
				<p style="text-align:center; color:red"><?= $msg ?></p>
				<form method="POST" action="connexion-validation.php">
					<table class="connect">
						<tr>
							<td>Nom d'utilisateur :</td>
							<td>
								<input type="text" name="utilisateur" value="">
							</td>
						</tr>
						<tr>
							<td>Mot de passe :</td>
							<td>
								<input type="password" name="pwd" value="">
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<input type="submit" value="Connexion">
							</td>
						</tr>
						<tr>
							<td colspan="2"><a href="inscription.php">Pas de compte? Créez en un!</a></td>

						</tr>
					</table>
				</form>
			</div>
		</div>

		<div id="info" class="coolfooter"></div>

		<div class="footerConnect">
			<div class="row">
				<div class="col-1">
					<img src="img/logo.png" style="width: 150px;">
				</div>
				<div class="col-10">
					<p>Posted by: ChicouTruck</p>
					<p>Contact information: ChicouTruck@bellnet.ca</p>
				</div>
			</div>
		</div>
	</div>
</body>

</html>