<!DOCTYPE html>
<html>

<head>
    <title>Connexion -- ChicouTruck</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="index.css" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="background">
        <nav class="navbar navbar-expand-sm ml-auto bg-dark navbar-dark sticky-top">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="ProjetDeFinDeSession.php"><img src="img/logo.png" style="width: 150px;"></a>
                </li>
            </ul>
        </nav>

    

        <div class="container" style="margin-top: 100px;">
            <div class="divConnect">
                <form method="POST" action="serviceUtilisateur.php">
                    <table class="connect">
                        <tr>
                            <td>Types de transport proposés :</td>
                            <td>
                            <div class="dropdown">
                                <div class="form-group">
                                <select class="form-control form-control-sm">
                                    <option>Transport dédié</option>
                                    <option>Transport général</option>
                                    <option>Transport réfrigéré</option>
                                </select>
                                </div> 
                            </td>
                            </td>
                        </tr>
                        <tr>
                            <td>mode de livraison :</td>
                            <td>
                            <div class="dropdown">
                                <div class="form-group">
                                <select class="form-control form-control-sm">
                                    <option>livraison de base</option>
                                    <option>livraison expresse</option>
                                    <option>livraison réfrigérer</option>
                                </select>
                                </div> 
                            </td>
                            </td>
                        </tr>
                        <tr>
                            <td>Adresse de depart :</td>
                            <td>
                                <input type="text" name="utilisateur" value="">
                            </td>
                        </tr>
                        <tr>
                            <td>Adresse d'arriver :</td>
                            <td>
                                <input type="text" name="utilisateur" value="">
                            </td>
                        </tr>
                        <tr>
                            <td>Recepteur :</td>
                            <td>
                                <input type="text" name="utilisateur" value="">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" value="Confirmer"/>
                            </td>
                            <td>
                                <!--<input type="button" value="Anuler" onclick="header('Location: serviceUtilisateur.php')"/>-->
                                <a href="serviceUtilisateur.php">Annuler</a>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>

        <br>

        <div class="container" style="margin-top: 100px;">
            <div class="divConnect">
                <form method="POST" action="serviceUtilisateur.php">
                    <table class="connect">
                        <tr>
                            <td>Type d'entreposage :</td>
                            <td>
                            <div class="dropdown">
                                <div class="form-group">
                                <select class="form-control form-control-sm">
                                    <option>Court terme</option>
                                    <option>Moyen terme</option>
                                    <option>Long terme</option>
                                </select>
                                </div> 
                            </td>
                            </td>
                        </tr>
                        <tr>
                            <td>Nom :</td>
                            <td>
                                <input type="text" name="utilisateur" value="">
                            </td>
                        </tr>
                        <tr>
                            <td>date debut :</td>
                            <td>
                                <input type="text" name="utilisateur" value="">
                            </td>
                        </tr>
                        <tr>
                            <td>date fin :</td>
                            <td>
                                <input type="text" name="utilisateur" value="">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" value="Confirmer"/>
                            </td>
                            <td>
                                <!--<input type="button" value="Anuler" onclick="header('Location: serviceUtilisateur.php')"/>-->
                                <a href="serviceUtilisateur.php">Annuler</a>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>


        <div id="info" class="coolfooter"></div>

        <div class="footerConnect">
            <div class="row">
                <div class="col-1">
                    <img src="img/logo.png" style="width: 150px;">
                </div>
                <div class="col-10">
                    <p>Posted by: ChicouTruck</p>
                    <p>Contact information: ChicouTruck@bellnet.ca</p>
                </div>
            </div>
        </div>
    </div>
</body>

</html>